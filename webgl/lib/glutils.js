
function getShader(gl,id){
	var shaderScript = document.getElementById(id);
	if (!shaderScript){
		return null;
	}

	var str = "";
	var k = shaderScript.firstChild;

	while(k){
		if (k.nodeType ==3){
			str += k.textContent;
		}
		k = k.nextSibling;
	}

	var shader;
	if (shaderScript.type == "x-shader/x-fragment"){
		shader = gl.createShader(gl.FRAGMENT_SHADER);
	} else if (shaderScript.type == "x-shader/x-vertex"){
		shader = gl.createShader(gl.VERTEX_SHADER);
	} else {
		return null;
	}

	gl.shaderSource(shader,str);
	gl.compileShader(shader);

	if (!gl.getShaderParameter(shader,gl.COMPILE_STATUS)){
		alert(gl.getShaderInfoLog(shader));
		return null;
	}

	return shader;
}

function getGLContext(){
	var canvas = document.getElementById("canvas-element-id");
	
	c_width = canvas.width;
		c_height = canvas.height;
	if (canvas == null){
		alert("there is no canvas on this page");
		return;
	}

	var names = ["webgl",
			 "experiment-webgl",
			 "webkit-3d",
			 "moz-webgl"];
	var ctx = null;
	for(var i = 0; i<names.length;++i){
		try{
			ctx = canvas.getContext(names[i]);
		}
		catch(e){}
		if (ctx) break;
	}
	
	if (ctx == null){
		alert("WebGL is not available");
	}else{
		return ctx;
	}
}

function loadFile(name,loader){
	var request = new XMLHttpRequest();
	var resource = 'http://' + document.domain+":8000" + name;
	request.open("GET",resource);
	request.onreadystatechange = function(){
		if (request.readyState == 4){
			if (request.status == 200 || (request.status == 0 &&
				document.domain.length == 0)){
					loader(name,JSON.parse(request.responseText));
			}
			else {
				alert('There was a problem loading the file :' + name);
				alert('HTML error code : ' + request.status);
			}			
		}
	}
	request.send();
}